<?php
session_start();

if (!isset($_SESSION['id'])) {
    $_SESSION['msg'] = "You must log in first";
    header('location: login.php');
}
if (isset($_GET['logout'])) {
    session_destroy();
    unset($_SESSION['id']);
    header("location: login.php");
}
// initializing variables
$username = "";
$email    = "";
$errors = array();

// connect to the database
$db = mysqli_connect('localhost', 'root', '', 'our_library');


// REGISTER USER
if (isset($_POST['add_book'])) {
    // receive all input values from the form
    $id = mysqli_real_escape_string($db, $_POST['id']);
    $author = mysqli_real_escape_string($db, $_POST['author']);
    $availabilty = mysqli_real_escape_string($db, $_POST['availability']);
    $m_id = mysqli_real_escape_string($db, $_POST['m_id']);
    $p_id = mysqli_real_escape_string($db, $_POST['p_id']);
    $title = mysqli_real_escape_string($db, $_POST['title']);

    $l_id=$_SESSION['id'];
    $query = "INSERT INTO `book`(`id`,`title`, `author`, `availability`, `m_id`, `p_id`, `l_id`) VALUES ('{$id}','{$title}','{$author}','{$availabilty}','{$m_id}','{$p_id}','{$l_id}')";
    $status = "";
    if (mysqli_query($db, $query)) {
        $status = "success";
    } else {
        $status = "Error description: " . mysqli_error($db);

    }
}

if (isset($_POST['add_pub'])) {
    // receive all input values from the form
    $id = mysqli_real_escape_string($db, $_POST['id']);
    $loc = mysqli_real_escape_string($db, $_POST['location']);
    $est_date = mysqli_real_escape_string($db, $_POST['est_date']);
    $f_name = mysqli_real_escape_string($db, $_POST['f_name']);
    $l_name = mysqli_real_escape_string($db, $_POST['l_name']);
    $query = "INSERT INTO `publisher`(`pub_id`, `location`, `est_date`, `f_name`, `l_name`) VALUES ('{$id}','{$loc}','{$est_date}','{$f_name}','{$l_name}')";
    $status = "";
    if (mysqli_query($db, $query)) {
        $status = "success";
    } else {
        $status = "Error description: " . mysqli_error($db);

    }
}

if (isset($_POST['add_mem'])) {
    // receive all input values from the form
    $id = mysqli_real_escape_string($db, $_POST['id']);
    $dob = mysqli_real_escape_string($db, $_POST['dob']);
    $gender = mysqli_real_escape_string($db, $_POST['gender']);
    $f_name = mysqli_real_escape_string($db, $_POST['f_name']);
    $l_name = mysqli_real_escape_string($db, $_POST['l_name']);
    $query = "INSERT INTO `member`(`id`, `f_name`, `l_name`, `DOB`, `gender`) VALUES ('{$id}','{$f_name}','{$l_name}','{$dob}','{$gender}')";
    $status = "";
    if (mysqli_query($db, $query)) {
        $status = "success";
    } else {
        $status = "Error description: " . mysqli_error($db);

    }
}


//

if (isset($_POST['del_book'])) {
// receive all input values from the form
    $id = mysqli_real_escape_string($db, $_POST['id']);

    $query="DELETE FROM `book` WHERE `id`='{$id}';";
    $status = "";
    if (mysqli_query($db, $query)) {
        $status = "success";
    } else {
        $status = "Error description: " . mysqli_error($db);

    }

}



if (isset($_POST['del_mem'])) {
// receive all input values from the form
    $id = mysqli_real_escape_string($db, $_POST['id']);

    $query="DELETE FROM `member` WHERE  `id`='{$id}';";
    $status = "";
    if (mysqli_query($db, $query)) {
        $status = "success";
    } else {
        $status = "Error description: " . mysqli_error($db);

    }

}







//1

if (isset($_POST['update_book'])) {
// receive all input values from the form
    $id = mysqli_real_escape_string($db, $_POST['id']);
    $author = mysqli_real_escape_string($db, $_POST['author']);
    $availabilty = mysqli_real_escape_string($db, $_POST['availability']);
    $m_id = mysqli_real_escape_string($db, $_POST['m_id']);
    $p_id = mysqli_real_escape_string($db, $_POST['p_id']);
    $l_id=mysqli_real_escape_string($db, $_SESSION['l_id']);
    $query = "UPDATE `book` SET `author`='{$author}',`availability`='{$availabilty}',`m_id`='{$m_id}',`p_id`='{$p_id}' WHERE `id`='{$id}';";
    $status = "";
    if (mysqli_query($db, $query)) {
        $status = "success";
    } else {
        $status = "Error description: " . mysqli_error($db);
    }
}


//
if (isset($_POST['update_mem'])) {
/// receive all input values from the form
    $id = mysqli_real_escape_string($db, $_POST['id']);
    $dob = mysqli_real_escape_string($db, $_POST['dob']);
    $gender = mysqli_real_escape_string($db, $_POST['gender']);
    $f_name = mysqli_real_escape_string($db, $_POST['f_name']);
    $l_name = mysqli_real_escape_string($db, $_POST['l_name']);
    $query = "UPDATE `member` SET `f_name`='{$f_name}',`l_name`='{$l_name}',`DOB`='{$dob}',`gender`='{$gender}' WHERE `id`='{$id}'";
    $status = "";
    if (mysqli_query($db, $query)) {
        $status = "success";
    } else {
        $status = "Error description: " . mysqli_error($db);
    }
}
//
if (isset($_POST['update_pub'])) {
// receive all input values from the form
    // receive all input values from the form
    $id = mysqli_real_escape_string($db, $_POST['id']);
    $loc = mysqli_real_escape_string($db, $_POST['location']);
    $est_date = mysqli_real_escape_string($db, $_POST['est_date']);
    $f_name = mysqli_real_escape_string($db, $_POST['f_name']);
    $l_name = mysqli_real_escape_string($db, $_POST['l_name']);
    $query = "UPDATE `publisher` SET `location`='{$loc}',`est_date`='{$est_date}',`f_name`='{$f_name}',`l_name`='{$l_name}'  WHERE `id`='{$id}';";
    $status = "";
    if (mysqli_query($db, $query)) {
        $status = "success";
    } else {
        $status = "Error description: " . mysqli_error($db);
    }
}

if (isset($_POST['borrow'])) {
// receive all input values from the form
    // receive all input values from the form
    $id = mysqli_real_escape_string($db, $_POST['id']);
    $m_id=mysqli_real_escape_string($db, $_POST['m_id']);
    $l_id=$_SESSION['id'];
    $availabilty=0;
    $query = "UPDATE `book` SET `availability`='{$availabilty}',`m_id`='{$m_id}',`l_id`='{$l_id}' WHERE `id`='{$id}' AND availability=1;";
    $status = "";
    if (mysqli_query($db, $query)) {
        $status = "success";
    } else {
        $status = "Error description: " . mysqli_error($db);
    }
}


if (isset($_POST['return'])) {
// receive all input values from the form
    // receive all input values from the form
    $id = mysqli_real_escape_string($db, $_POST['id']);
    $m_id="";
    $l_id=$_SESSION['id'];
    $availabilty=1;
    $query = "UPDATE `book` SET `availability`='{$availabilty}',`m_id`='{$m_id}',`l_id`='{$l_id}' WHERE `id`='{$id}' AND availability=0;";
    $status = "";
    if (mysqli_query($db, $query)) {
        $status = "success";
    } else {
        $status = "Error description: " . mysqli_error($db);
    }
}


mysqli_close($db);


?>
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top">
    <div class="container">
        <a class="navbar-brand" href="#">
            <!--            <img src="http://placehold.it/150x50?text=Logo" alt="">-->
            <h1> Library for people<h1>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <!--            <ul class="navbar-nav ml-auto">-->
            <!--                <li class="nav-item active">-->
            <!--                    <a class="nav-link" href="#">Home-->
            <!--                        <span class="sr-only">(current)</span>-->
            <!--                    </a>-->
            <!--                </li>-->
            <!--                <li class="nav-item">-->
            <!--                    <a class="nav-link" href="#">About</a>-->
            <!--                </li>-->
            <!--                <li class="nav-item">-->
            <!--                    <a class="nav-link" href="#">Services</a>-->
            <!--                </li>-->
            <!--                <li class="nav-item">-->
            <!--                    <a class="nav-link" href="#">Contact</a>-->
            <!--                </li>-->
            <!--            </ul>-->

            <?php if (isset($_SESSION['success'])) : ?>
                <div class="error success" >
                    <h3>
                        <?php
                        echo $_SESSION['success'];
                        unset($_SESSION['success']);
                        ?>
                    </h3>
                </div>
            <?php endif ?>

            <!-- logged in user information -->
            <?php  if (isset($_SESSION['id'])) : ?>
                <p>Welcome <strong><?php echo $_SESSION['id']; ?></strong></p>
                <p> <a href="index.php?logout='1'" style="color: red;">logout</a> </p>
            <?php endif ?>

        </div>
    </div>
</nav>

<!-- Page Content -->
<div class="container">
    <h1 class="mt-4">Read and enrich yourself.</h1>
    <p>

        <?php
        echo $status;
        ?>

    </p>
</div>
<!-- /.container -->


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
</body>
</html>