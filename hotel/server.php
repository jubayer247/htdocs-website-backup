<?php
session_start();

// initializing variables
$id = "";
$errors = array();

// connect to the database
$db = mysqli_connect('localhost', 'root', '', 'hotel');

// REGISTER USER
if (isset($_POST['reg_user'])) {
    // receive all input values from the form
    $id = mysqli_real_escape_string($db, $_POST['id']);
    $name = mysqli_real_escape_string($db, $_POST['name']);
    $city = mysqli_real_escape_string($db, $_POST['city']);
    $street = mysqli_real_escape_string($db, $_POST['street']);
    $house = mysqli_real_escape_string($db, $_POST['house']);
    $phone = mysqli_real_escape_string($db, $_POST['phone']);
    $c_room_no = mysqli_real_escape_string($db, $_POST['c_room_no']);
    $password_1 = mysqli_real_escape_string($db, $_POST['password_1']);
    $password_2 = mysqli_real_escape_string($db, $_POST['password_2']);


    // form validation: ensure that the form is correctly filled ...
    // by adding (array_push()) corresponding error unto $errors array
    if (empty($id)) { array_push($errors, "Username is required"); }
   // if (empty($email)) { array_push($errors, "Email is required"); }
    if (empty($password_1)) { array_push($errors, "Password is required"); }
    if ($password_1 != $password_2) {
        array_push($errors, "The two passwords do not match");
    }

    // first check the database to make sure
    // a user does not already exist with the same username and/or email
    $user_check_query = "SELECT * FROM customer WHERE id='$id' LIMIT 1";
    $result = mysqli_query($db, $user_check_query);
    $user = mysqli_fetch_assoc($result);

    if ($user) { // if user exists
        if ($user['id'] === $id) {
            array_push($errors, "Username already exists");
        }
//
//        if ($user['na'] === $email) {
//            array_push($errors, "email already exists");
//        }
    }

    // Finally, register user if there are no errors in the form
    if (count($errors) == 0) {
        $password = md5($password_1);//encrypt the password before saving in the database

        $query = "INSERT INTO `customer`(`id`, `name`, `city`, `street`, `house`, `phone`, `c_room_no`, `password`) VALUES ('{$id}','{$name}','{$city}','{$street}','{$house}','{$phone}','{$c_room_no}','{$password}')";
        mysqli_query($db, $query);
        $_SESSION['id'] = $id;
        $_SESSION['success'] = "You are now logged in";
        header('location: home.php');
    }
}

if (isset($_POST['login_user'])) {
    $id = mysqli_real_escape_string($db, $_POST['id']);
    $password = mysqli_real_escape_string($db, $_POST['password']);

    if (empty($id)) {
        array_push($errors, "Username is required");
    }
    if (empty($password)) {
        array_push($errors, "Password is required");
    }

    if (count($errors) == 0) {
        $password = md5($password);
        $query = "SELECT * FROM `customer`  WHERE `id`='{$id}' AND `password`='{$password}'";
        $results = mysqli_query($db, $query);
        if (mysqli_num_rows($results) == 1) {
            $_SESSION['id'] = $id;
            $_SESSION['success'] = "You are now logged in";
            header('location: home.php');
        }else {
            array_push($errors, "Wrong username/password combination");
        }
    }
}


if (isset($_POST['login_admin'])) {
    $id = mysqli_real_escape_string($db, $_POST['id']);
    $password = mysqli_real_escape_string($db, $_POST['password']);

    if (empty($id)) {
        array_push($errors, "Username is required");
    }
    if (empty($password)) {
        array_push($errors, "Password is required");
    }

    if (count($errors) == 0) {
        $password = md5($password);
        $query = "SELECT * FROM `admin` WHERE `id`='{$id}' AND `password`='{$password}'";
        $results = mysqli_query($db, $query);
        if (mysqli_num_rows($results) == 1) {
            $_SESSION['id'] = $id;
            $_SESSION['success'] = "You are now logged in";
            header('location: adminArea.php');
        }else {
            array_push($errors, "Wrong username/password combination");
        }
    }
}

?>
