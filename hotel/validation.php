<?php
session_start();

if (!isset($_SESSION['id'])) {
    $_SESSION['msg'] = "You must log in first";
    header('location: index.php');
}
if (isset($_GET['logout'])) {
    session_destroy();
    unset($_SESSION['id']);
    header("location: index.php");
}
$username = "";
$email    = "";
$errors = array();
// connect to the database
$db = mysqli_connect('localhost', 'root', '', 'hotel');


// REGISTER USER
if (isset($_POST['staff_add'])) {
    // receive all input values from the form
    $name = mysqli_real_escape_string($db, $_POST['name']);
    $ssn = mysqli_real_escape_string($db, $_POST['ssn']);
    $city = mysqli_real_escape_string($db, $_POST['city']);
    $street = mysqli_real_escape_string($db, $_POST['street']);
    $house = mysqli_real_escape_string($db, $_POST['house']);
    $wage = mysqli_real_escape_string($db, $_POST['wage']);
    $phone = mysqli_real_escape_string($db, $_POST['phone']);
    $m_id = mysqli_real_escape_string($db, $_POST['m_id']);
    $query = "INSERT INTO `staff`(`name`, `ssn`, `city`, `street`, `house`, `phone`, `wages`, `m_id`) VALUES ('{$name}','{$ssn}','{$city}','{$street}','{$house}','{$phone}','{$wage}','{$m_id}')";
    $status = "";

    if (mysqli_query($db, $query)) {
        $status = "success";
    } else {
        $status = "Error description: " . mysqli_error($db);

    }
}

if (isset($_POST['manager_add'])) {
    // receive all input values from the form
    $name = mysqli_real_escape_string($db, $_POST['name']);
    $city = mysqli_real_escape_string($db, $_POST['city']);
    $street = mysqli_real_escape_string($db, $_POST['street']);
    $phone = mysqli_real_escape_string($db, $_POST['phone']);
    $m_id = mysqli_real_escape_string($db, $_POST['m_id']);
    $house = mysqli_real_escape_string($db, $_POST['house']);
    $query = "INSERT INTO `manager`(`m_id`, `name`, `city`, `street`, `house`, `phone`) VALUES ('{$m_id}','{$name}','{$city}','{$street}','{$house}','{$phone}')";
    $status = "";

    if (mysqli_query($db, $query)) {
        $status = "success";
    } else {
        $status = "Error description: " . mysqli_error($db);

    }
}

if (isset($_POST['service_add'])) {
    // receive all input values from the form
    $cust_id = mysqli_real_escape_string($db, $_POST['id']);
    $ser_name = mysqli_real_escape_string($db, $_POST['ser_name']);
    $query = "INSERT INTO `service`(`s_name`, `c_id`) VALUES ('{$ser_name}','{$cust_id}')";
    $status = "";

    if (mysqli_query($db, $query)) {
        $status = "success";
    } else {
        $status = "Error description: " . mysqli_error($db);

    }
}

if (isset($_POST['assign_stuff'])) {
    // receive all input values from the form
    $ssn = mysqli_real_escape_string($db, $_POST['ssn']);
    $room_no = mysqli_real_escape_string($db, $_POST['no']);
    $query = "INSERT INTO `maintenence`(`ssn`, `room_no`) VALUES ('{$ssn}','{$room_no}')";
    $status = "";

    if (mysqli_query($db, $query)) {
        $status = "success";
    } else {
        $status = "Error description: " . mysqli_error($db);

    }
}


if (isset($_POST['room_add'])) {
    // receive all input values from the form
    $floor = mysqli_real_escape_string($db, $_POST['floor']);
    $side= mysqli_real_escape_string($db, $_POST['side']);
    $number = mysqli_real_escape_string($db, $_POST['no']);
    $availability = mysqli_real_escape_string($db, $_POST['availability']);
    $query = "INSERT INTO `room`(`floor`, `side`, `number`, `availability`) VALUES ('{$floor}','{$side}','{$number}',{$availability})";
    $status = "";
    if (mysqli_query($db, $query)) {
        $status = "success";
    } else {
        $status = "Error description: " . mysqli_error($db);

    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// REGISTER USER
if (isset($_POST['stuff_update'])) {
    // receive all input values from the form
    $name = mysqli_real_escape_string($db, $_POST['name']);
    $ssn = mysqli_real_escape_string($db, $_POST['ssn']);
    $city = mysqli_real_escape_string($db, $_POST['city']);
    $street = mysqli_real_escape_string($db, $_POST['street']);
    $house = mysqli_real_escape_string($db, $_POST['house']);
    $wage = mysqli_real_escape_string($db, $_POST['wage']);
    $phone = mysqli_real_escape_string($db, $_POST['phone']);
    $m_id = mysqli_real_escape_string($db, $_POST['m_id']);
    $query = "UPDATE `staff` SET `name`='{$name}',`city`='{$city}',`street`='{$street}',`house`='{$house}',`phone`='{$phone}',`wages`='{$wage}',`m_id`='{$m_id}' WHERE `ssn` ='{$ssn}'";
    $status = "";

    if (mysqli_query($db, $query)) {
        $status = "success";
    } else {
        $status = "Error description: " . mysqli_error($db);

    }
}

if (isset($_POST['cust_update'])) {
    // receive all input values from the form
    $name = mysqli_real_escape_string($db, $_POST['name']);
    $id = mysqli_real_escape_string($db, $_POST['id']);
    $city = mysqli_real_escape_string($db, $_POST['city']);
    $street = mysqli_real_escape_string($db, $_POST['street']);
    $house = mysqli_real_escape_string($db, $_POST['house']);
    $c_room_no = mysqli_real_escape_string($db, $_POST['room_no']);
    $phone = mysqli_real_escape_string($db, $_POST['phone']);
    //$m_id = mysqli_real_escape_string($db, $_POST['m_id']);
    $query = "UPDATE `customer` SET `name`='{$name}',`city`='{$city}',`street`='{$street}',`house`='{$house}',`phone`='{$phone}',`c_room_no`='{$c_room_no}' WHERE `id` ='{$id}'";
    $status = "";

    if (mysqli_query($db, $query)) {
        $status = "success";
    } else {
        $status = "Error description: " . mysqli_error($db);

    }
}


if (isset($_POST['manager_update'])) {
    // receive all input values from the form
    $name = mysqli_real_escape_string($db, $_POST['name']);
    $city = mysqli_real_escape_string($db, $_POST['city']);
    $street = mysqli_real_escape_string($db, $_POST['street']);
    $phone = mysqli_real_escape_string($db, $_POST['phone']);
    $m_id = mysqli_real_escape_string($db, $_POST['m_id']);
    $house = mysqli_real_escape_string($db, $_POST['house']);
    $query = "UPDATE `manager` SET `name`='{$name}',`city`='{$city}',`street`='{$street}',`house`='{$house}',`phone`='{$phone}' WHERE `m_id`='{$m_id}'";
    $status = "";

    if (mysqli_query($db, $query)) {
        $status = "success";
    } else {
        $status = "Error description: " . mysqli_error($db);

    }
}




if (isset($_POST['room_update'])) {
    // receive all input values from the form
    $floor = mysqli_real_escape_string($db, $_POST['floor']);
    $side= mysqli_real_escape_string($db, $_POST['side']);
    $number = mysqli_real_escape_string($db, $_POST['no']);
    $availability = mysqli_real_escape_string($db, $_POST['availability']);
    $query = "UPDATE `room` SET `floor`='{$floor}',`side`='{$side}',`availability`='{$availability}' WHERE  `number`='{$number}'";
    $status = "";
    if (mysqli_query($db, $query)) {
        $status = "success";
    } else {
        $status = "Error description: " . mysqli_error($db);

    }
}




/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


if (isset($_POST['staff_delete'])) {
// receive all input values from the form
    $ssn = mysqli_real_escape_string($db, $_POST['ssn']);

    $query="DELETE FROM `staff` WHERE  `ssn`='{$ssn}';";
    $status = "";
    if (mysqli_query($db, $query)) {
        $status = "success";
    } else {
        $status = "Error description: " . mysqli_error($db);

    }

}





if (isset($_POST['cust_delete'])) {
// receive all input values from the form
    $id = mysqli_real_escape_string($db, $_POST['id']);

    $query = "DELETE FROM `customer` WHERE `id`='{$id}';";
    $status = "";
    if (mysqli_query($db, $query)) {
        $status = "success";
    } else {
        $status = "Error description: " . mysqli_error($db);

    }
}

if (isset($_POST['room_delete'])) {
// receive all input values from the form
    $number = mysqli_real_escape_string($db, $_POST['no']);

    $query = "DELETE FROM `room` WHERE `number`='{$number}';";
    $status = "";
    if (mysqli_query($db, $query)) {
        $status = "success";
    } else {
        $status = "Error description: " . mysqli_error($db);

    }
}



if (isset($_POST['manager_delete'])) {
// receive all input values from the form
    $m_id = mysqli_real_escape_string($db, $_POST['m_id']);

    $query = "DELETE FROM `manager` WHERE `m_id`='{$m_id}';";
    $status = "";
    if (mysqli_query($db, $query)) {
        $status = "success";
    } else {
        $status = "Error description: " . mysqli_error($db);

    }
}


if (isset($_POST['release_stuff'])) {
// receive all input values from the form
    $ssn = mysqli_real_escape_string($db, $_POST['ssn']);
    $room_no = mysqli_real_escape_string($db, $_POST['no']);
    $query = "DELETE FROM `maintenence` WHERE  `ssn`='{$ssn}' and `room_no`='{$room_no}';";
    $status = "";
    if (mysqli_query($db, $query)) {
        $status = "success";
    } else {
        $status = "Error description: " . mysqli_error($db);

    }
}



if (isset($_POST['service_remove'])) {
// receive all input values from the form
    $id = mysqli_real_escape_string($db, $_POST['id']);
    $ser_name = mysqli_real_escape_string($db, $_POST['ser_name']);
    $query = "DELETE FROM `service` WHERE  `s_name`='{$ser_name}' and `c_id`='{$id}';";
    $status = "";
    if (mysqli_query($db, $query)) {
        $status = "success";
    } else {
        $status = "Error description: " . mysqli_error($db);

    }
}





mysqli_close($db);


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top">
    <div class="container">
        <a class="navbar-brand" href="#">
            <!--            <img src="http://placehold.it/150x50?text=Logo" alt="">-->
            <h1> Hotel Transilvania <h1>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <!--            <ul class="navbar-nav ml-auto">-->
            <!--                <li class="nav-item active">-->
            <!--                    <a class="nav-link" href="#">Home-->
            <!--                        <span class="sr-only">(current)</span>-->
            <!--                    </a>-->
            <!--                </li>-->
            <!--                <li class="nav-item">-->
            <!--                    <a class="nav-link" href="#">About</a>-->
            <!--                </li>-->
            <!--                <li class="nav-item">-->
            <!--                    <a class="nav-link" href="#">Services</a>-->
            <!--                </li>-->
            <!--                <li class="nav-item">-->
            <!--                    <a class="nav-link" href="#">Contact</a>-->
            <!--                </li>-->
            <!--            </ul>-->

            <?php if (isset($_SESSION['success'])) : ?>
                <div class="error success" >
                    <h3>
                        <?php
                        echo $_SESSION['success'];
                        unset($_SESSION['success']);
                        ?>
                    </h3>
                </div>
            <?php endif ?>

            <!-- logged in user information -->
            <?php  if (isset($_SESSION['id'])) : ?>
                <p>Welcome <strong><?php echo $_SESSION['id']; ?></strong></p>
                <p> <a href="index.php?logout='1'" style="color: red;">logout</a> </p>
            <?php endif ?>

        </div>
    </div>
</nav>

<!-- Page Content -->
<div class="container">
    <h1 class="mt-4">Admin area </h1>
    <p>

        <?php
        echo $status;
        ?>



    </p>
</div>
<!-- /.container -->


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
</body>
</html>