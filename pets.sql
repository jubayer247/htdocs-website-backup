CREATE TABLE pets (
       _id INTEGER PRIMARY KEY AUTOINCREMENT,
       name TEXT NOT NULL,
       breed TEXT,
       gender INTEGER NOT NULL,
       weight INTEGER NOT NULL DEFAULT 0);

  INSERT INTO pets (name, breed, gender, weight) VALUES ( "Tommy", "Pomeranian", 1, 4);
  INSERT INTO pets (name, breed, gender, weight) VALUES ("Garfield", "Tabby", 1, 14);
  INSERT INTO pets (name, breed, gender, weight) VALUES ("Binx", "Bombay", 1, 6);
  INSERT INTO pets (name, breed, gender, weight)  VALUES ( "Lady", "Cocker Spaniel", 2, 14);
  INSERT INTO pets (name, breed, gender, weight) VALUES ("Duke", "Unknown", 1, 70);
  INSERT INTO pets (name, breed, gender, weight) VALUES ("Cat", "Tabby", 0, 7);
  INSERT INTO pets (name, breed, gender, weight) VALUES ("Baxter", "Border Terrier", 1, 8);
  INSERT INTO pets (name, gender, weight) VALUES ("Arlene", 2, 5);